var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var mongoose = require('mongoose');

var BasicStrategy = require('passport-http').BasicStrategy;

module.exports = function (passport) {

    passport.use('basic', new BasicStrategy(
        function (login, password, done) {
            User.findOne({ login: login }, function (err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false); }
                if (!user.validPassword(password)) { return done(null, false); }
                return done(null, user);
            });
        }
    ));

    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================

    passport.use('local-signup', new LocalStrategy({
        usernameField: 'login',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, login, password, done) {
            process.nextTick(function () {
                User.findOne({ 'login': login }, function (err, user) {
                    if (err)
                        return done(err);

                    if (user && !user.isDisabled) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken'));
                    }
                    else if (req.body.password !== req.body.password2)
                        return done(null, false, req.flash('signupMessage', 'Passwords do not match'));
                    else if (req.body.password.length < 5)
                        return done(null, false, req.flash('signupMessage', 'Password length < 5'));
                    else if (req.body.login.length < 4)
                        return done(null, false, req.flash('signupMessage', 'Login length < 4'));
                    else if (req.body.fullname.length < 2)
                        return done(null, false, req.flash('signupMessage', 'Fullname length < 2'));
                    else {
                        let newUser = new User();

                        newUser._id = new mongoose.Types.ObjectId();
                        newUser.login = login;
                        newUser.password = newUser.generateHash(password);
                        newUser.fullname = req.body.fullname;
                        newUser.biography = '';
                        newUser.role = 0;
                        newUser.registeredAt = new Date().toISOString();
                        newUser.avaUrl = "https://res.cloudinary.com/hhiefmflq/image/upload/v1572597465/sample.jpg";
                        newUser.isDisabled = false;
                        newUser.articlesId = [];
                        newUser.companiesId = [];

                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }

                });

            });

        }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================

    passport.use('local-login', new LocalStrategy({
        usernameField: 'login',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, login, password, done) {
            User.findOne({ 'login': login, 'isDisabled': false }, function (err, user) {
                if (err)
                    return done(err);

                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found')); 

                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Wrong password'));

                return done(null, user);
            });

        }));

};