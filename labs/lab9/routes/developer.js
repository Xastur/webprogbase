const express = require('express');
const router = express.Router();
const passport = require('../app');

router.get('/', passport.authenticate('basic', { session: false }), function (req, res) {
    console.log(req.user);
    if (req.user.role != 1)
        return res.status(403).send("Forbidden");
    res.status(200).render('dev', {
        title: 'Dev',
        homeId: 'home',
        usersId: 'users',
        blogsId: 'blogs',
        companiesId: 'companies',
        aboutId: 'about',
    });
})

module.exports = router;