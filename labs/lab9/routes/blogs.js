const express = require('express');
let multer = require("multer");
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;
var mongoose = require('mongoose');
const dbconfig = require('../config/database');
const cloudinary = require('cloudinary');
const Article = require('../models/article');
const User = require('../models/user');

const router = express.Router();

var storage = multer.diskStorage({})
var upload = multer({ storage: storage })

var paginationAmount = 2;

cloudinary.config({
    cloud_name: dbconfig.cloud_name,
    api_key: dbconfig.api_key,
    api_secret: dbconfig.api_secret
});

router.get('/blogs/new', isLoggedIn, (req, res) => {
    let userLogin;
    let admin = false;
    if (req.user) {
        userLogin = req.user.login;
        if (req.user.role === 1)
            admin = true;
    }
    res.render('newBlog', {
        title: "New BLog",
        homeId: 'home',
        usersId: 'users',
        blogsId: 'blogsCur',
        companiesId: 'companies',
        aboutId: 'about',
        user: req.user,
        userLogin: userLogin,
        admin: admin,
        pageId: req.user._id
    });
});

router.post('/blogs/new', upload.single('ImageSrc'), (req, res) => {
    const date = new Date();
    date.toISOString();
    const file = req.file;
    if (!file || (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') || req.body.ArtTitle === '') {
        console.log("Not enough info")
        return res.redirect('/blogs/new');
    }

    cloudinary.v2.uploader.upload(file.path, function (err, data) {
        if (err) {
            console.log(err);
            res.redirect('/');
        }

        let newBlog = new Article();
        newBlog._id = new mongoose.Types.ObjectId();
        newBlog.artTitle = req.body.ArtTitle;
        newBlog.userId = req.user._id;
        newBlog.companyId = null;
        newBlog.aboutMyCar = req.body.AboutMyCar;
        newBlog.passportData = req.body.passportData;
        newBlog.likesAmount = 0;
        newBlog.viewsAmount = 0;
        newBlog.date = date;
        newBlog.imageSrc = data.url;

        console.log(newBlog);
        Article.insert(newBlog)
            .then(x => {
                return User.update(req.user._id, { $push: { 'articlesId': x._id } })
            })
            .then(x => res.redirect(newBlog._id))
            .catch(err => {
                res.redirect('/error/' + err);
            })
    })
});

router.post('/blogs/:id', (req, res) => {
    const blogId = req.params.id;
    Article.delete(blogId)
        .then(x => {
            return User.update(req.user._id, { $pull: { 'articlesId': x._id } })
        })
        .then(x => res.redirect('/blogs?page=1'))
        .catch(err => {
            res.redirect('/error/' + err);
        });
});

// const path = require('path');
// const fileOptions = { root: path.join(__dirname, '../views')};

router.get('/blogs', isLoggedIn, (req, res) => {
    // res.setHeader("Content-Type", mime.lookup(url)); //Solution!
    // res.set('Content-type', 'text/html');
    // res.sendFile('blogs.html', fileOptions);

    const currPage = parseInt(req.params.id);
    let prevPage = 1;
    let nextPage = currPage;
    let pageAm = 0;
    let userLogin;
    let admin;

    if (req.query.search === undefined || req.query.search === '') {
        Article.getAmount()
            .then(amount => {
                pageAm = Math.ceil(amount / paginationAmount);
                if (pageAm === 0)
                    pageAm += 1;
                if (pageAm > currPage) {
                    nextPage = currPage + 1;
                }
                if (currPage > 1) {
                    prevPage = currPage - 1;
                }

                Article.getAllPaginated((currPage - 1) * paginationAmount, paginationAmount)
                    .then(blogs => {
                        if (req.user) {
                            userLogin = req.user.login;
                            if (req.user.role === 1)
                                admin = true;
                        }
                        // res.sendFile('blogs.mst', fileOptions);
                        res.render('blogs', {
                            title: 'Blogs',
                            homeId: 'home',
                            usersId: 'users',
                            blogsId: 'blogsCur',
                            companiesId: 'companies',
                            aboutId: 'about',
                            page: currPage,
                            nextPage: nextPage,
                            prevPage: prevPage,
                            blogs: blogs,
                            pageAmount: pageAm,
                            user: req.user,
                            userLogin: userLogin,
                            admin: admin,
                            pageId: req.user._id
                        });
                    })
                    .catch(err => {
                        res.redirect('/error/' + err);
                    })
            })
            .catch(err => {
                res.redirect('/error/' + err);
            })
    }
    else {
        let blogs = new Array;
        Article.getAll()
            .then(arts => {
                if (req.user) {
                    userLogin = req.user.login;
                    if (req.user.role === 1)
                        admin = true;
                }

                if (arts) {
                    arts.forEach(element => {
                        if (element.artTitle.indexOf(req.query.search) > -1) {
                            blogs.push(element);
                        }
                    });
                    res.render('blogs', {
                        title: 'Blogs',
                        homeId: 'home',
                        usersId: 'users',
                        blogsId: 'blogsCur',
                        companiesId: 'companies',
                        aboutId: 'about',
                        page: 1,
                        blogs: blogs,
                        nextPage: nextPage,
                        prevPage: prevPage,
                        pageAmount: 1,
                        user: req.user,
                        userLogin: userLogin,
                        admin: admin,
                        pageId: req.user._id
                    });
                }
            })
            .catch(err => {
                res.redirect('/error/' + err);
            })
    }
});

router.get('/blogs/:id', isLoggedIn, (req, res) => {
    const blogId = req.params.id;

    Article.getArticle(blogId)
        .then(art => {
            let userLogin;
            let admin = false;
            if (req.user) {
                userLogin = req.user.login;
                if (req.user.role === 1)
                    admin = true;
            }

            User.findById(art.userId)
                .then(creator => {
                    if (JSON.stringify(creator._id) === JSON.stringify(req.user._id))
                        admin = true;

                    res.render('blog', {
                        title: 'Blog' + blogId,
                        homeId: 'home',
                        usersId: 'users',
                        blogsId: 'blogsCur',
                        companiesId: 'companies',
                        aboutId: 'about',
                        article: art,
                        id: ObjectId(blogId),
                        user: req.user,
                        userLogin: userLogin,
                        admin: admin,
                        pageId: req.user._id,
                        creator: creator
                    });
                })
                .catch(err => res.redirect('/error/' + err))

        })
        .catch(err => {
            res.redirect('/error/' + err);
        })
});

router.get('/blogs/:id/update', isLoggedIn, (req, res) => {
    const blogId = req.params.id;
    Article.getArticle(blogId)
        .then(article => {
            let userLogin = req.user.login;
            let admin = false;

            if (req.user.role === 1)
                admin = true;

            res.render('updateBlog', {
                title: article.artTitle,
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogsCur',
                companiesId: 'companies',
                aboutId: 'about',
                article: article,
                user: req.user,
                userLogin: userLogin,
                admin: admin,
                pageId: req.user._id
            })
        })
        .catch(err => {
            res.redirect('/error/' + err);
        })
})

router.post('/blogs/:id/update', upload.single('ImageSrc'), (req, res) => {
    const blogId = req.params.id;
    let updateInfo;
    const file = req.file;
    if (file) {
        cloudinary.v2.uploader.upload(file.path, function (err, data) {
            if (err) {
                console.log(err);
                res.redirect('/');
            }

            updateInfo = {
                imageSrc: data.url,
                artTitle: req.body.ArtTitle,
                passportData: req.body.PassportData,
                aboutMyCar: req.body.AboutMyCar
            }

            Article.update(blogId, updateInfo)
                .then(x => {
                    console.log("Updated company");
                    res.redirect('/blogs/' + blogId);
                })
                .catch(err => {
                    res.redirect('/error/' + err);
                })
        })
    }
    else {
        updateInfo = {
            artTitle: req.body.ArtTitle,
            passportData: req.body.PassportData,
            aboutMyCar: req.body.AboutMyCar
        }

        Article.update(blogId, updateInfo)
            .then(x => {
                console.log("Updated company");
                res.redirect('/blogs/' + blogId);
            })
            .catch(err => {
                res.redirect('/error/' + err);
            })
    }
})

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/auth/login');
}

module.exports = router;