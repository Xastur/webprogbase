var mongoose = require('mongoose');
const pmongo = require('promised-mongo');
// const ObjectId = pmongo.ObjectId;

var CompanySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    companyName: String,
    admins: Array,
    articlesId: Array,
    aboutCompany: String,
    date: Date,
    imageSrc: String
});

let CompanyModel = mongoose.model('Company', CompanySchema);

class Company {
    constructor() {}

    static getAll() {
        return CompanyModel.find();
    }

    static getCompany(id) {
        return CompanyModel.findById(id);
    }

    static getAllByUserId(id) {
        return CompanyModel.find({ admins: id })
    }

    static getAllPaginated(skip, limit) {
        return CompanyModel.find(null, null, { skip, limit });
    }

    static insert(company) {
        return new CompanyModel(company).save();
    }

    static delete(id) {
        return CompanyModel.findByIdAndDelete(id);
    }

    static update(id, updateInfo) {
        return CompanyModel.findOneAndUpdate({_id: id}, updateInfo);
    }
}

module.exports = Company;