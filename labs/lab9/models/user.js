var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var UserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    login: String,
    password: String,
    fullname: String,
    biography: String,
    role: Number,
    registeredAt: Date,
    avaUrl: String,
    isDisabled: Boolean,
    articlesId: Array,
    companiesId: Array,
});

UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function (password) {
    console.log(password, this.password);
    return bcrypt.compareSync(password, this.password);
};

const UserModel = mongoose.model('User', UserSchema);

UserModel.getAll = function () {
    return UserModel.find();
}

UserModel.getAllPaginated = function (skip, limit) {
    return UserModel.find(null, null, { skip, limit });
}

UserModel.getById = function (id) {
    return UserModel.findById(id);
}

UserModel.getByLogin = function (login) {
    return UserModel.find({ login: login });
}

UserModel.insert = function (user) {
    return new UserModel(user).save();
}

UserModel.delete = function (id) {
    return UserModel.findByIdAndDelete(id);
}

UserModel.update = function (id, updateInfo) {
    return UserModel.findOneAndUpdate({ _id: id }, updateInfo);
}

module.exports = mongoose.model('User', UserSchema);