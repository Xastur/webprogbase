const users = require('../data/users.json');

class User 
{
    constructor(id, login, role, fullname, registeredAt) {
        this.id = id; // number
        this.login = login; // string
        this.fullname = fullname; // string
        this.role = role; // number
        this.registeredAt = registeredAt; // ISO 8601
        this.avaUrl; // str
        this.isDisabled = false; // bool
    }

    static getById(id)
    {
        let getUser = null;
        users.items.forEach(user => {
            if (user.id === id)
            {
                getUser = new User(user.id, user.login, user.role, user.fullname, user.registeredAt);
            }
        });
        return getUser;
    }

    static getAll()
    {
        let arr = new Array;
        users.items.forEach(user => {
            arr.push(user);
        });
        return arr;
    }
};

module.exports = User;