const fs = require('fs');
const articles = require("../data/articles.json");

class Article
{
    constructor(id, title, author, likesAmount, viewsAmount, date) {
        this.id = id; // number
        this.title = title; // string
        this.author = author; // string
        this.likesAmount = likesAmount; // number
        this.viewsAmount = viewsAmount; // number
        this.date = date; // ISO 8601
    }

    static insert(title, author, likesAmount, viewsAmount, date)
    {
        const newArticle = new Article(articles.nextId, title, author, likesAmount, viewsAmount, date);
        articles.items.push(newArticle);
        articles.nextId++;
        fs.writeFileSync("./data/articles.json", JSON.stringify(articles, null, 4));
        return articles.nextId - 1;
    }

    static getAll()
    {
        let arr = new Array;
        articles.items.forEach(article => {
            arr.push(article);
        });
        return arr;
    }

    static getById(id)
    {
        let getArticle = null;
        articles.items.forEach(article => {
            if (article.id === id)
            {
                getArticle = new Article(article.id, article.title, article.author, article.likesAmount, article.viewsAmount);
            }
        });
        return getArticle;
    }

    static update(id, title, author, likesAmount, viewsAmount)
    {
        articles.items.forEach(article => {
            if (article.id === id)
            {
                article.title = title;
                article.author = author;
                article.likesAmount = likesAmount;
                article.viewsAmount = viewsAmount;
            }
        });
        fs.writeFileSync("./data/articles.json", JSON.stringify(articles, null, 4));
    }

    static deleteById(id)
    {
        let i = 0; 
        articles.items.forEach(article => {
            if (article.id === id)
            {
                articles.items.splice(i, 1);
            }
            i++;
        });
        fs.writeFileSync("./data/articles.json", JSON.stringify(articles, null, 4));
    }
};

module.exports = Article;