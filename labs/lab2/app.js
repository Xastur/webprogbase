const user = require('./models/user');
const article = require('./models/article');
const readline = require("readline-sync"); 

while (true)
{
    const inputString = readline.question("> ");
    //
    if (inputString === "users")
    {
        let arr = user.getAll();

        console.log("\n");
        arr.forEach(userr => {
            console.log("login: " + userr.login);
            console.log("fullname: " + userr.fullname);
        });
        console.log("\n");
    }    
    else if (inputString.substring(0, 10) === "users/get/")
    {
        let num = inputString.substring(inputString.length, 10);
        let usr = user.getById(parseInt(num));
        if (usr !== null)
        {
            console.log("\nid: " + usr.id);
            console.log("login: " + usr.login);
            console.log("fullname: " + usr.fullname);
            console.log("registeredAt: " + usr.registeredAt + "\n");
        }
        else 
        {
            console.log("\nno such user found\n");
        }
    }
    else if (inputString.substring(0, 13) === "articles/get/")
    {
        let num = inputString.substring(inputString.length, 13);
        let art = article.getById(parseInt(num));
        if (art !== null)
        {
            console.log("\nid: " + art.id);
            console.log("title: " + art.title);
            console.log("author: " + art.author);
            console.log("likesAmount: " + art.likesAmount);
            console.log("viewsAmount: " + art.viewsAmount + "\n");
        }
        else 
        {
            console.log("\nno such article found\n");
        }
    }
    else if (inputString.substring(0, 16) === "articles/delete/")
    {
        let num = inputString.substring(inputString.length, 16);
        let art = article.getById(parseInt(num));
        if (art !== null)
        {
            article.deleteById(art.id);
            console.log("\narticle with id " + art.id + " was deleted successfully\n");
        }
        else 
        {
            console.log("\nno such article found\n");
        }   
    }
    else if (inputString.substring(0, 16) === "articles/update/")
    {
        let num = inputString.substring(inputString.length, 16);
        let art = article.getById(parseInt(num));
        if (art !== null)
        {
            const newTitle = readline.question("Enter new title: ");
            const newAuthor = readline.question("Enter new author: ");
            const newLikesAmount = readline.question("Enter new likes amount: ");
            const newViewsAmount = readline.question("Enter new views amount: ");

            article.update(art.id, newTitle, newAuthor, newLikesAmount, newViewsAmount);
            console.log("\narticle with id " + art.id + " was updated successfully\n");
        }
        else 
        {
            console.log("\nno such article found\n");
        }   
    }
    else if (inputString === "articles/insert")
    {
        const title = readline.question("Enter title: ");
        const author = readline.question("Enter author: ");
        const likesAmount = readline.question("Enter likes amount: ");
        const viewsAmount = readline.question("Enter views amount: ");
        const date = new Date();
        date.toISOString();

        const id = article.insert(title, author, likesAmount, viewsAmount, date);
        console.log("\nNew article id: " + id + "\n");
    }
    else if (inputString === "articles")
    {
        let arr = article.getAll();

        console.log("\n");
        arr.forEach(art => {
            console.log("title: " + art.title);
            console.log("author: " +art.author);
        });
        console.log("\n");
    }
    else if (inputString === "exit")
        break;
}