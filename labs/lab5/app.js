const express = require('express');
const consolidate = require('consolidate');
const path = require('path');
const routeIndex = require('./routes/index');
const routeUsers = require('./routes/users');
const routeBlogs = require('./routes/blogs');
const routeCompanies = require('./routes/companies');
const routeAbout = require('./routes/about');
const mustache = require('mustache-express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const databaseUrl = 'mongodb://localhost:27017/db';
const conOptions = { useNewUrlParser: true }

const app = express();
const serverPort = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
mongoose.set('useFindAndModify', false);

app.use(express.static('public'));
app.use(express.static('data'));

const viewsDir = path.join(__dirname, 'views');
const partialsDir = path.join(viewsDir, 'partials');
app.engine('html', consolidate.swig);
app.engine('mst', mustache(partialsDir));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use('/', routeIndex);
app.use('/', routeUsers);
app.use('/', routeBlogs);
app.use('/', routeCompanies);
app.use('/', routeAbout);

mongoose.connect(databaseUrl, conOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(serverPort, () => console.log(`Listening on port ${serverPort}`)))
    .catch(err => console.log(`Error: ${err}`))