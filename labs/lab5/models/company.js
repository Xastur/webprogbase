var mongoose = require('mongoose');

var CompanySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    companyName: String,
    admins: Array,
    articlesId: Array,
    aboutCompany: String,
    date: Date,
    imageSrc: String
});

let CompanyModel = mongoose.model('Company', CompanySchema);

class Company {
    constructor(_id, companyName, admins, articlesId, aboutCompany, date, imageSrc) {
        this._id = _id;
        this.companyName = companyName;
        this.admins = admins;
        this.articlesId = articlesId;
        this.aboutCompany = aboutCompany;
        this.date = date;
        this.imageSrc = imageSrc;
    }

    static getAll() {
        return CompanyModel.find();
    }

    static getCompany(id) {
        return CompanyModel.findById(id);
    }

    static insert(company) {
        return new CompanyModel(company).save();
    }

    static delete(id) {
        return CompanyModel.findByIdAndDelete(id);
    }

    static update(id, updateInfo) {
        return CompanyModel.findOneAndUpdate({_id: id}, updateInfo);
    }
}

module.exports = Company;