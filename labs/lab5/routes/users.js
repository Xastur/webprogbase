const express = require('express');
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;
const User = require('../models/user');

const router = express.Router();

router.get('/public/images/:name', (req, res) => {
    const path = "/public/images/" + req.params.name;
    res.sendFile('/home/dima/webprogbase/labs/lab5/' + path);
})

router.get('/users', (req, res) => {
    User.getAll()
        .then(users => {
            res.render('users', {
                title: 'Users',
                homeId: 'home',
                usersId: 'usersCur',
                blogsId: 'blogs',
                companiesId: 'companies',
                aboutId: 'about',
                users: users
            });
        })
        .catch(err => {
            console.log(err);
            res.redirect('/');
        })
});

router.get('/users/:id', (req, res) => {
    const userId = req.params.id;
    User.getUser(userId)
        .then(user => {
            res.render('user', {
                title: user.login,
                homeId: 'home',
                usersId: 'usersCur',
                blogsId: 'blogs',
                companiesId: 'companies',
                aboutId: 'about',
                imageSrc: "../public/images/" + user.avaUrl,
                user: user
            });
        })
        .catch(err => {
            console.log(err);
            res.redirect('/');
        })
});

module.exports = router;