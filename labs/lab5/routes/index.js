const express = require('express');
const router = express.Router();

router.get('/', function(req, res, next) {
    res.render('index', {title: 'Home',
                         homeId: 'homeCur',
                         usersId: 'users',
                         blogsId: 'blogs',
                         companiesId: 'companies',
                         aboutId: 'about'});
});

module.exports = router;