const express = require('express');
let multer = require("multer");
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;
var mongoose = require('mongoose');
const config = require('../config');
const cloudinary = require('cloudinary');
const Article = require('../models/article');

const router = express.Router();

// SET STORAGE
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/home/dima/webprogbase/labs/lab5/data/fs')
    },
    filename: function (req, file, cb) {
        if (file.mimetype === 'image/png')
            cb(null, Date.now() + '.png')
        if (file.mimetype === 'image/jpeg')
            cb(null, Date.now() + '.jpg')
    }
})

var upload = multer({ storage: storage })

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

router.get('/data/fs/:name', (req, res) => {
    const path = "/data/fs/" + req.params.name;
    res.sendFile('/home/dima/webprogbase/labs/lab5/' + path);
})

router.get('/blogs/new', (req, res) => {
    res.render('newBlog', {
        title: "New BLog",
        homeId: 'home',
        usersId: 'users',
        blogsId: 'blogsCur',
        companiesId: 'companies',
        aboutId: 'about',
    });
});

router.post('/blogs/new', upload.single('ImageSrc'), (req, res) => {
    const date = new Date();
    date.toISOString();
    const file = req.file;
    if (!file || (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') || req.body.ArtTitle === '') {
        console.log("Not enough info");
        return res.redirect('/blogs/new');
    }

    let newBlog = new Article(
        new mongoose.Types.ObjectId(),
        req.body.ArtTitle,
        null,
        null,
        req.body.AboutMyCar,
        req.body.PassportData,
        0,
        0,
        date,
        file.filename
    );
    console.log(req.body);
    Article.insert(newBlog)
        .then(x => res.redirect(newBlog._id))
        .catch(err => {
            console.log("Error: " + err);
            res.redirect('/');
        })
});

router.post('/blogs/:id', (req, res) => {
    const blogId = req.params.id;
    Article.delete(blogId)
        .then(res => console.log('Successfully deleted an article'))
        .catch(err => { console.log(err) });

    res.redirect("/blogspage=1");
});

router.get('/blogs?page=:id', (req, res) => {
    const currPage = parseInt(req.params.id);
    let prevPage = 1;
    let nextPage = currPage;
    let pageAm = 0;
    Article.getAmount()
        .then(amount => {
            pageAm = Math.ceil(amount / 2);
            if (pageAm === 0)
                pageAm += 1;
            if (pageAm > currPage) {
                nextPage = currPage + 1;
            }
        })
    if (currPage > 1) {
        prevPage = currPage - 1;
    }
    if (req.query.search === undefined || req.query.search === '') {
        Article.getAllPaginated((currPage - 1) * 2, 2)
            .then(blogs => {
                res.render('blogs', {
                    title: 'Blogs',
                    homeId: 'home',
                    usersId: 'users',
                    blogsId: 'blogsCur',
                    companiesId: 'companies',
                    aboutId: 'about',
                    page: currPage,
                    nextPage: nextPage,
                    prevPage: prevPage,
                    blogs: blogs,
                    pageAmount: pageAm
                });
            })
            .catch(err => console.log("Error: " + err))
    }
    else {
        let blogs = new Array;
        Article.getAll()
            .then(arts => {
                if (arts) {
                    arts.forEach(element => {
                        if (element.artTitle.indexOf(req.query.search) > -1) {
                            blogs.push(element);
                        }
                    });
                    res.render('blogs', {
                        title: 'Blogs',
                        homeId: 'home',
                        usersId: 'users',
                        blogsId: 'blogsCur',
                        companiesId: 'companies',
                        aboutId: 'about',
                        page: 1,
                        blogs: blogs,
                        nextPage: nextPage,
                        prevPage: prevPage,
                        pageAmount: 1
                    });
                }
            })
            .catch(err => {
                console.log(err);
                res.redirect('/');
            })
    }
});

router.get('/blogs/:id', (req, res) => {
    const blogId = req.params.id;
    Article.getArticle(blogId)
        .then(art => {
            res.render('blog', {
                title: 'Blog' + blogId,
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogsCur',
                companiesId: 'companies',
                aboutId: 'about',
                article: art,
                imageSrc: "../data/fs/" + art.imageSrc,
                id: ObjectId(blogId)
            });
        })
        .catch(err => {
            console.log(err);
            res.redirect('/');
        })
});

router.get('/blogs/:id/update', (req, res) => {
    const blogId = req.params.id;
    Article.getArticle(blogId)
        .then(article => {
            res.render('updateBlog', {
                title: article.artTitle,
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogsCur',
                companiesId: 'companies',
                aboutId: 'about',
                article: article,
            })
        })
})

router.post('/blogs/:id/update', upload.single('ImageSrc'), (req, res) => {
    const blogId = req.params.id;
    let updateInfo;
    if (req.file) {
        updateInfo = {
            imageSrc: req.file.filename,
            artTitle: req.body.ArtTitle,
            passportData: req.body.PassportData,
            aboutMyCar: req.body.AboutMyCar
        }
    }
    else {
        updateInfo = {
            artTitle: req.body.ArtTitle,
            passportData: req.body.PassportData,
            aboutMyCar: req.body.AboutMyCar
        }
    }

    Article.update(blogId, updateInfo)
        .then(x => {
            console.log("Updated company");
            res.redirect('/blogs/' + blogId);
        })
        .catch(err => {
            console.log("Error: " + err);
            res.redirect('/');
        })
})

module.exports = router;