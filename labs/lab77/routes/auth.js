let multer = require("multer");
const cloudinary = require('cloudinary');
const dbconfig = require('../config/database');
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;
var storage = multer.diskStorage({})
var upload = multer({ storage: storage })

const User = require('../models/user');
const Article = require('../models/article');
const Company = require('../models/company');

cloudinary.config({
    cloud_name: dbconfig.cloud_name,
    api_key: dbconfig.api_key,
    api_secret: dbconfig.api_secret
});

module.exports = function (app, passport) {

    app.post('/auth/signup', passport.authenticate('local-signup', {
        failureRedirect: '/auth/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }), (req, res) => {
        res.redirect(`/user/${req.user._id}`);
    });

    app.post('/auth/login', passport.authenticate('local-login', {
        failureRedirect: '/auth/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }), (req, res) => {
        res.redirect(`/user/${req.user._id}`);
    });

    app.get('/auth/signup', function (req, res) {
        let userLogin;
        let admin = false;
        if (req.user) {
            userLogin = req.user.login;
            if (req.user.role === 1)
                admin = true;
        }
        // render the page and pass in any flash data if it exists
        res.render('signup', {
            error: req.flash('signupMessage'),
            title: "Sign up",
            homeId: 'home',
            usersId: 'users',
            blogsId: 'blogs',
            companiesId: 'companies',
            aboutId: 'about',
            userLogin: userLogin,
            admin: admin,
        });
    });

    app.get('/auth/login', function (req, res) {
        let userLogin;
        let admin = false;
        if (req.user) {
            res.redirect('/user/' + req.user._id);
        }
        else {
            res.render('login', {
                error: req.flash('loginMessage'),
                title: "Log in",
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogs',
                companiesId: 'companies',
                aboutId: 'about',
                userLogin: userLogin,
                admin: admin,
            })
        }
    });

    app.post('/user/:id/delete', isLoggedIn, function (req, res) {
        const userId = req.params.id;
        User.update(userId, { "isDisabled": true })
            .then(x => {
                console.log('User deleted successfully');
                req.logout();
                res.redirect('/');
            })
            .catch(err => {
                console.log(err);
                res.redirect('/error/' + err);
            })
    })

    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    app.get('/user/:id', isLoggedIn, function (req, res) {
        const userId = req.params.id;
        let pageId = req.user._id;
        let userLogin = req.user.login;
        let usersPage = false;
        let admin = false;

        if (userId == req.user._id) {
            usersPage = true;
        }

        if (req.user.role === 1) {
            admin = true;
            usersPage = true;
        }

        User.findById(userId)
            .then(user => {
                res.render('user', {
                    title: user.login,
                    homeId: 'home',
                    usersId: 'usersCur',
                    blogsId: 'blogs',
                    companiesId: 'companies',
                    aboutId: 'about',
                    user: user,
                    userLogin: userLogin,
                    admin: admin,
                    usersPage: usersPage,
                    pageId: pageId
                });
            })
            .catch(err => res.redirect('/error/' + err));

    });

    app.post('/user/:id/changerole', checkAdmin, function (req, res) {
        const userId = req.params.id;
        let newRole;
        User.findById(userId)
            .then(user => {
                if (user.role === 1)
                    newRole = 0;
                else
                    newRole = 1;
                return User.update(userId, { 'role': newRole });
            })
            .then(x => {
                console.log("Role changed!");
                if (userId == req.user._id)
                    res.redirect('/');
                else
                    res.redirect('/user/' + userId);
            })
            .catch(err => {
                console.log(err);
                res.redirect('/');
            })

    })

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.post('/auth/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/users', checkAdmin, (req, res) => {
        User.getAll()
            .then(users => {
                let userLogin;
                let admin = false;
                if (req.user) {
                    userLogin = req.user.login;
                    if (req.user.role === 1)
                        admin = true;
                }

                res.render('users', {
                    title: 'Users',
                    homeId: 'home',
                    usersId: 'usersCur',
                    blogsId: 'blogs',
                    companiesId: 'companies',
                    aboutId: 'about',
                    users: users,
                    user: req.user,
                    userLogin: userLogin,
                    admin: admin,
                    pageId: req.user._id
                });
            })
            .catch(err => {
                console.log(err);
                res.redirect('/');
            })
    });

    app.get('/user/:id/update', isLoggedIn, (req, res) => {
        const userId = req.params.id;
        let error;
        if (req.query.error)
            error = req.query.error;
        if (userId != req.user._id && req.user.role === 0) {
            console.log(userId);
            console.log(req.user._id);
            console.log("WRONG ID");
            res.redirect('/user/' + userId);
        }
        else {
            let admin = false;
            let userLogin = req.user.login;
            if (req.user.role === 1)
                admin = true;

            User.findById(userId)
                .then(user => {
                    res.render('updateUser', {
                        title: 'Update User',
                        homeId: 'home',
                        usersId: 'usersCur',
                        blogsId: 'blogs',
                        companiesId: 'companies',
                        aboutId: 'about',
                        user: user,
                        userLogin: userLogin,
                        admin: admin,
                        error: error,
                        pageId: req.user._id
                    })
                })
                .catch(err => res.redirect('/error/' + err));

        }
    })

    app.post('/user/:id/update', upload.single('imageSrc'), (req, res) => {
        const userId = req.params.id;
        let updateInfo;
        const file = req.file;
        if (file) {
            cloudinary.v2.uploader.upload(file.path, function (err, data) {
                if (err) {
                    console.log(err);
                    res.redirect('/error/' + err);
                }
                else {
                    if (req.body.fullname.length < 2) {
                        //error
                        req.flash('updateMessage', 'Fullname length < 2');
                        res.redirect('/user/' + userId + '/update');
                    }
                    else {
                        updateInfo = {
                            avaUrl: data.url,
                            fullname: req.body.fullname,
                            biography: req.body.biography
                        }

                        User.update(userId, updateInfo)
                            .then(x => {
                                console.log("User updated");
                                res.redirect('/user/' + userId);
                            })
                            .catch(err => res.redirect('/error/' + err));
                    }
                }
            })
        }
        else {
            if (req.body.fullname.length < 2) {
                //error
                // req.flash('updateMessage', 'Fullname length < 2');
                res.redirect('/user/' + userId + '/update?error=Fullname+length+<+2');
            }
            else {
                updateInfo = {
                    fullname: req.body.fullname,
                    biography: req.body.biography
                }

                User.update(userId, updateInfo)
                    .then(x => {
                        console.log("User updated");
                        res.redirect('/user/' + userId);
                    })
                    .catch(err => res.redirect('/error/' + err));
            }
        }
    })

    app.get('/user/:id/blogs', isLoggedIn, (req, res) => {
        const userId = req.params.id;
        let admin = false;
        let userLogin = req.user.login;
        if (req.user.role === 1)
            admin = true;

        Article.getAllByUserId(userId)
            .then(blogs => {
                res.render('userBlogs', {
                    title: 'User blogs',
                    homeId: 'home',
                    usersId: 'users',
                    blogsId: 'blogsCur',
                    companiesId: 'companies',
                    aboutId: 'about',
                    user: req.user,
                    blogs: blogs,
                    userLogin: userLogin,
                    pageId: req.user._id, 
                    admin: admin
                })
            })
            .catch(err => res.redirect('/error/' + err))
    })

    app.get('/user/:id/companies', isLoggedIn, (req, res) => {
        const userId = req.params.id;
        let admin = false;
        let userLogin = req.user.login;
        if (req.user.role === 1)
            admin = true;
        Company.getAllByUserId(ObjectId(userId))
            .then(comp => {
                res.render('userCompanies', {
                    title: 'User companies',
                    homeId: 'home',
                    usersId: 'users',
                    blogsId: 'blogs',
                    companiesId: 'companiesCur',
                    aboutId: 'about',
                    user: req.user,
                    companies: comp,
                    userLogin: userLogin,
                    pageId: req.user._id, 
                    admin: admin
                })
            })
            .catch(err => res.redirect('/error/' + err))
    })

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();

        res.redirect('/auth/login');
    }

    function checkAdmin(req, res, next) {
        if (!req.user)
            res.redirect('/auth/login');
        // res.sendStatus(401); // 'Not authorized'
        else if (req.user.role !== 1) {
            // res.sendStatus(403); // 'Forbidden'
            res.redirect('/error/403');
        }
        else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
    }
}