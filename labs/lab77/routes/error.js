module.exports = function (app) {

    app.get('/error/:id', function (req, res) {
        let userLogin;
        let admin = false;
        if (req.user) {
            userLogin = req.user.login;
            if (req.user.role === 1)
                admin = true;
        }
        res.render('error', {
            title: "Error", 
            _id: req.params.id,
            homeId: 'home',
            usersId: 'users',
            blogsId: 'blogs',
            companiesId: 'companies',
            aboutId: 'about',
            admin: admin, 
            pageId: req.user._id, 
            userLogin: userLogin
        })
    })
}