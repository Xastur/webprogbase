// const express = require('express');
// const router = express.Router();

module.exports = function (app) {
    app.get('/about', function (req, res, next) {
        let userLogin;
        let pageId;
        let admin = false;
        if (req.user)
        {
            userLogin = req.user.login;
            pageId = req.user._id;
            if (req.user.role === 1)
                admin = true;
        }
        res.render('about', {
            title: 'About',
            homeId: 'home',
            usersId: 'users',
            blogsId: 'blogs',
            companiesId: 'companies',
            aboutId: 'aboutCur',
            user: req.user,
            userLogin: userLogin, 
            admin: admin, 
            pageId: pageId
        });
    });
}
// module.exports = router;