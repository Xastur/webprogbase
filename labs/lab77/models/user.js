var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var UserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    login: String,
    password: String,
    fullname: String,
    biography: String,
    role: Number,
    registeredAt: Date,
    avaUrl: String,
    isDisabled: Boolean,
    articlesId: Array,
    companiesId: Array,
});

// const UserModel = mongoose.model('User', UserSchema);

// class User 
// {
//     constructor(_id, login, password, role, fullname, avaUrl, registeredAt, companiesId) {
//         this._id = _id;
//         this.login = login; // string
//         this.password = password;
//         this.fullname = fullname; // string
//         this.role = role; // number
//         this.registeredAt = registeredAt; // ISO 8601
//         this.avaUrl = avaUrl; // str
//         this.isDisabled = false; // bool
//         this.companiesId = companiesId;
//         this.password = "password";
//     }

//     UserModel.getAll() {
//         return UserModel.find();
//     }

//     UserModel.getUser(id) {
//         return UserModel.findById(id);
//     }

//     UserModel.getUserByLogin(login) {
//         return UserModel.find( {login: login} );
//     }

//     UserModel.insert(user) {
//         return new UserModel(user).save();
//     }

//     UserModel.delete(id) {
//         return UserModel.findByIdAndDelete(id);
//     } 

//     UserModel.update(id, updateInfo) {
//         return UserModel.findOneAndUpdate({_id: id}, updateInfo);
//     }

//     UserModel.generateHash(password) {
//         return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
//     };

//     UserModel.validPassword(password) {
//         return bcrypt.compareSync(password, this.password);
//     };
// };

UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function (password) {
    console.log(password, this.password);
    return bcrypt.compareSync(password, this.password);
};

const UserModel = mongoose.model('User', UserSchema);

UserModel.getAll = function () {
    return UserModel.find();
}

UserModel.getById = function (id) {
    return UserModel.findById(id);
}

UserModel.getByLogin = function (login) {
    return UserModel.find({ login: login });
}

UserModel.insert = function (user) {
    return new UserModel(user).save();
}

UserModel.delete = function (id) {
    return UserModel.findByIdAndDelete(id);
}

UserModel.update = function (id, updateInfo) {
    return UserModel.findOneAndUpdate({ _id: id }, updateInfo);
}

module.exports = mongoose.model('User', UserSchema);