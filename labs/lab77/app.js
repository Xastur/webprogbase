const express        = require('express');
const consolidate    = require('consolidate');
const path           = require('path');
const routeIndex     = require('./routes/index');
const routeBlogs     = require('./routes/blogs');
const routeCompanies = require('./routes/companies');
const mustache       = require('mustache-express');
const bodyParser     = require('body-parser');
const mongoose       = require('mongoose');
const dbconfig       = require('./config/database');
const passport       = require('passport');
const flash          = require('connect-flash');
const morgan         = require('morgan');
const cookieParser   = require('cookie-parser');
const session        = require('express-session');

const databaseUrl = dbconfig.DatabaseUrl;
const serverPort = dbconfig.ServerPort;
const conOptions = { useNewUrlParser: true }

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

require('./config/passport')(passport); // pass passport for configuration

// required for passport
app.use(session({
	secret: "Some_secret_string",
	resave: false,
	saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash mes

app.use(express.static('public'));
app.use(express.static('data'));

const viewsDir = path.join(__dirname, 'views');
const partialsDir = path.join(viewsDir, 'partials');
app.engine('html', consolidate.swig);
app.engine('mst', mustache(partialsDir));
app.set('views', viewsDir);
app.set('view engine', 'mst');

require('./routes/auth')(app, passport); // load our routes and pass in our app and fully configured passport
require('./routes/about')(app);
require('./routes/error')(app);

app.use('/', routeIndex);
app.use('/', routeBlogs);
app.use('/', routeCompanies);

mongoose.connect(databaseUrl, conOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(serverPort, () => console.log(`Listening on port ${serverPort}`)))
    .catch(err => console.log(`Error: ${err}`))