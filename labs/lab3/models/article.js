const fs = require('fs');
const articles = require("../data/articles.json");

class Article
{
    constructor(id, artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount, date, imageSrc) {
        this.id = id; // number
        this.artTitle = artTitle; // string
        this.author = author; // string
        this.aboutMyCar = aboutMyCar; // string
        this.passportData = passportData; // string
        this.likesAmount = likesAmount; // number
        this.viewsAmount = viewsAmount; // number
        this.date = date; // ISO 8601
        this.imageSrc = imageSrc;
    }

    static insert(artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount, date, imageSrc)
    {
        const newArticle = new Article(articles.nextId, artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount, date, imageSrc);
        articles.items.push(newArticle);
        articles.nextId++;
        fs.writeFileSync("./data/articles.json", JSON.stringify(articles, null, 4));
        return articles.nextId - 1;
    }

    static getAll()
    {
        let arr = new Array;
        articles.items.forEach(article => {
            arr.push(article);
        });
        return arr;
    }

    static getById(id)
    {
        let getArticle = null;
        articles.items.forEach(article => {
            if (article.id === id)
            {
                getArticle = new Article(article.id, article.artTitle, article.author, article.aboutMyCar, article.passportData, article.likesAmount, article.viewsAmount, article.date, article.imageSrc);
            }
        });
        return getArticle;
    }

    static update(id, artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount)
    {
        articles.items.forEach(article => {
            if (article.id === id)
            {
                article.artTitle = artTitle;
                article.author = author;
                article.aboutMyCar = aboutMyCar;
                article.passportData = passportData;
                article.likesAmount = likesAmount;
                article.viewsAmount = viewsAmount;
            }
        });
        fs.writeFileSync("./data/articles.json", JSON.stringify(articles, null, 4));
    }

    static deleteById(id)
    {
        let i = 0; 
        articles.items.forEach(article => {
            if (article.id === id)
            {
                articles.items.splice(i, 1);
            }
            i++;
        });
        fs.writeFileSync("./data/articles.json", JSON.stringify(articles, null, 4));
    }
};

module.exports = Article;