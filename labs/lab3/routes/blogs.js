const express = require('express');
const article = require('../models/article');
const articlesJSON = require('../data/articles');
const router = express.Router();

router.get('/blogs', (req, res) => {
    res.render('blogs', {title: 'Blogs',
                         homeId: 'home',
                         usersId: 'users',
                         blogsId: 'blogsCur',
                         aboutId: 'about',
                         blogs: articlesJSON});
});

router.get('/blogs/:id', (req, res) => {
    const art = article.getById(parseInt(req.params.id));
    if (art === null)
    {
        console.log("Error: wrong article id");
        res.status(404).send("Error 404");
    }
    else 
    {
        console.log("No error in getById");
        res.render('blog', {title: 'Blog' + req.params.id,
                                            homeId: 'home',
                                            usersId: 'users',
                                            blogsId: 'blogsCur',
                                            aboutId: 'about',
                                            article: art});
    }
});

module.exports = router;