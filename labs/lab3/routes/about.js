const express = require('express');
const router = express.Router();

router.get('/about', function(req, res) {
    res.render('about', {title: 'About',
                         homeId: 'home',
                         usersId: 'users',
                         blogsId: 'blogs',
                         aboutId: 'aboutCur'});
});

module.exports = router;