const express = require('express');
const users = require('../models/user');
const usersJSON = require('../data/users');
const router = express.Router();

router.get('/users', function(req, res) {
    res.render('users', {title: 'Users',
                         homeId: 'home',
                         usersId: 'usersCur',
                         blogsId: 'blogs',
                         aboutId: 'about',
                         users: usersJSON});
});

router.get('/users/:id', (req, res) => {
    const user = users.getById(parseInt(req.params.id));
    if (user === null)
    {
        console.log("Error: wrong user id");
        res.status(404).send("Error 404");
    }
    else 
    {
        console.log("No error in getById");
        res.render('user', {title: 'User' + req.params.id,
                                            homeId: 'home',
                                            usersId: 'usersCur',
                                            blogsId: 'blogs',
                                            aboutId: 'about',
                                            user: user});
    }
});

module.exports = router;