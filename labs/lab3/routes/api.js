const express = require('express');
const users = require('../models/user');
const router = express.Router();

router.get('/users', function(req, res) {
    res.end(JSON.stringify(users.getAll(), null, 4));
});

router.get('/users/:id', function(req, res) {
    const user = users.getById(parseInt(req.params.id));
    if (user === null)
    {
        console.log("Error: wrong user id");
        res.status(404).send("Error 404");
    }
    else 
    {
        console.log("No error in getById");
        res.end(JSON.stringify(user, null, 4));
    }
});

module.exports = router;