const express = require('express');
const consolidate = require('consolidate');
const path = require('path');
const routeIndex = require('./routes/index');
const routeUsers = require('./routes/users');
const routeBlogs = require('./routes/blogs');
const routeAbout = require('./routes/about');
const routeApi = require('./routes/api');
const mustache = require('mustache-express');

const app = express();
const port = 3000;

app.use(express.static('public'));
app.listen(port, () => console.log(`Listening on port ${port}`));

// view engine setup
const viewsDir = path.join(__dirname, 'views');
const partialsDir = path.join(viewsDir, 'partials');
app.engine('html', consolidate.swig);
app.engine('mst', mustache(partialsDir));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use('/', routeIndex);
app.use('/', routeUsers);
app.use('/', routeBlogs);
app.use('/', routeAbout);
app.use('/api', routeApi);