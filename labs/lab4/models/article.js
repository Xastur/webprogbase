const fs = require('fs');
const articles = require("../data/articles.json");

class Article {
    constructor(id, artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount, date, imageSrc) {
        this.id = id; // number
        this.artTitle = artTitle; // string
        this.author = author; // string
        this.aboutMyCar = aboutMyCar; // string
        this.passportData = passportData; // string
        this.likesAmount = likesAmount; // number
        this.viewsAmount = viewsAmount; // number
        this.date = date; // ISO 8601
        this.imageSrc = imageSrc;
    }

    static insert(artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount, date, imageSrc, callback) {
        process.nextTick(() => {
            const newArticle = new Article(articles.nextId, artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount, date, imageSrc);
            articles.items.push(newArticle);
            articles.nextId++;
            articles.amount++;
            fs.writeFile("./data/articles.json", JSON.stringify(articles, null, 4), function (err) {
                if (err) {
                    // console.log(err);
                    callback(null, new Error("Error in writefile"));
                }
                callback(articles.nextId - 1, null);
            })
        });
    }

    static getAll(callback) {
        process.nextTick(() => {
            let arr = new Array;
            articles.items.forEach(article => {
                arr.push(article);
            });
            callback(arr, null);
        });
    }

    static getById(id, callback) {
        process.nextTick(() => {
            let getArticle = null;
            articles.items.forEach(article => {
                if (article.id === id) {
                    getArticle = new Article(article.id, article.artTitle, article.author, article.aboutMyCar, article.passportData, article.likesAmount, article.viewsAmount, article.date, article.imageSrc);
                }
            });
            if (getArticle == null) {
                callback(null, new Error("Wrong article id"));
            }
            else {
                callback(getArticle, null);
            }
        });
    }

    static update(id, artTitle, author, aboutMyCar, passportData, likesAmount, viewsAmount, callback) {
        process.nextTick(() => {
            let updated = false;
            articles.items.forEach(article => {
                if (article.id === id) {
                    article.artTitle = artTitle;
                    article.author = author;
                    article.aboutMyCar = aboutMyCar;
                    article.passportData = passportData;
                    article.likesAmount = likesAmount;
                    article.viewsAmount = viewsAmount;
                    updated = true;
                }
            });
            if (updated === false) {
                // console.log("Wrong id in update");
                callback(null, new Error("Wrong article id!!"));
            }
            else {
                fs.writeFile("./data/articles.json", JSON.stringify(articles, null, 4), function (err) {
                    if (err) {
                        // console.log(err);
                        callback(null, err);
                    }
                })
            }
        });
    }

    static deleteById(id, callback) {
        process.nextTick(() => {
            let i = 0;
            let deleted = false;
            articles.items.forEach(article => {
                if (article.id === id) {
                    articles.items.splice(i, 1);
                    deleted = true;
                }
                i++;
            });
            if (deleted === false) {
                callback(null, new Error("Wrong article id!"));
            }
            else {
                articles.amount--;
                fs.writeFile("./data/articles.json", JSON.stringify(articles, null, 4), function (err) {
                    if (err) {
                        // console.log(err);
                        callback(null, err);
                    }
                })
            }
        });
    }
};

module.exports = Article;