const users = require('../data/users.json');

class User 
{
    constructor(id, login, role, fullname, avaSrc, registeredAt) {
        this.id = id; // number
        this.login = login; // string
        this.fullname = fullname; // string
        this.role = role; // number
        this.registeredAt = registeredAt; // ISO 8601
        this.avaSrc = avaSrc; // str
        this.isDisabled = false; // bool
    }

    static getById(id, callback)
    {
        process.nextTick(() => {
            let getUser = null;
            users.items.forEach(user => {
                if (user.id === id)
                {
                    getUser = new User(user.id, user.login, user.role, user.fullname, user.avaSrc, user.registeredAt);
                }
            });
            if (getUser == null)
            {
                callback(null, new Error("Wrong user id"));
            }
            else 
            {
                callback(getUser, null);
            }
        });
    }

    static getAll(callback)
    {
        process.nextTick(() => {
            let arr = new Array;
            users.items.forEach(user => {
                arr.push(user);
            });
            callback(arr, null);
        });
    }
};

module.exports = User;