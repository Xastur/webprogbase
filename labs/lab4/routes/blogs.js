const express = require('express');
const article = require('../models/article');
const articlesJSON = require('../data/articles');
let multer = require("multer");

const router = express.Router();

// SET STORAGE
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/home/dima/webprogbase/labs/lab4/data/fs')
    },
    filename: function (req, file, cb) {
        if (file.mimetype === 'image/png')
            cb(null, Date.now() + '.png')
        if (file.mimetype === 'image/jpeg')
            cb(null, Date.now() + '.jpg')
    }
})

var upload = multer({ storage: storage })

router.get('/data/fs/:name', (req, res) => {
    const path = "/data/fs/" + req.params.name;
    res.sendFile('/home/dima/webprogbase/labs/lab4/' + path);
})

router.get('/blogs/new', (req, res) => {
    res.render('newBlog', {
        title: "New BLog",
        homeId: 'home',
        usersId: 'users',
        blogsId: 'blogsCur',
        aboutId: 'about'
    });
});

router.post('/blogs/new', upload.single('ImageSrc'), (req, res) => {
    const date = new Date();
    date.toISOString();
    const file = req.file;
    if (!file) {
        console.log("No file");
        return res.redirect('/blogs/new');
    }

    article.insert(req.body.ArtTitle, req.body.Author, req.body.AboutMyCar, req.body.PassportData, 0, 0, date, file.filename, function (result, err) {
        if (err) {
            console.log("Error in insert");
            res.redirect('/');
        }
        else {
            res.redirect(result);
        }
    });
});

router.post('/blogs/:id', (req, res) => {
    article.deleteById(parseInt(req.params.id), function (result, err) {
        if (err) {
            console.log(err.toString());
            
        }
        else 
        {
            res.redirect("/blogspage=1");
        }
    });
   
});

router.get("/blogspage=:id", (req, res) => {
    let blogs = new Array;
    const currPage = parseInt(req.params.id);
    let nextPage = currPage;
    let prevPage = 1;
    let pageAm = Math.ceil(parseInt(articlesJSON.amount) / 2);
    if (pageAm === 0) {
        pageAm += 1;
    }
    if (pageAm > currPage) {
        nextPage = currPage + 1
    }
    if (currPage > 1) {
        prevPage = currPage - 1;
    }
    if (req.query.search === undefined || req.query.search === '') {
        article.getAll(function (allBlogs, err) {
            if (err) {
                console.log(err.toString());
            }
            else {
                for (let i = (currPage - 1) * 2; i < currPage * 2; i++) {
                    if (i < allBlogs.length) {
                        blogs.push(allBlogs[i]);
                    }
                    if (i === currPage * 2 - 1) {
                        res.render('blogs', {
                            title: 'Blogs',
                            homeId: 'home',
                            usersId: 'users',
                            blogsId: 'blogsCur',
                            aboutId: 'about',
                            page: currPage,
                            nextPage: nextPage,
                            prevPage: prevPage,
                            blogs: blogs,
                            pageAmount: pageAm
                        });
                    }
                }
            }
        });
    }
    else {
        article.getAll(function (arts, err) {
            if (arts) {
                arts.forEach(element => {
                    if (element.artTitle.indexOf(req.query.search) > -1) {
                        blogs.push(element);
                    }
                });
                res.render('blogs', {
                    title: 'Blogs',
                    homeId: 'home',
                    usersId: 'users',
                    blogsId: 'blogsCur',
                    aboutId: 'about',
                    page: 1,
                    nextPage: nextPage,
                    prevPage: prevPage,
                    blogs: blogs,
                    pageAmount: 1
                });
            }
        });
    }
});

router.get('/blogs/:id', (req, res) => {
    article.getById(parseInt(req.params.id), function (art, err) {
        if (err) {
            console.log(err.toString());
            res.status(404).send(err.toString());
        }
        else {            
            res.render('blog', {
                title: 'Blog' + req.params.id,
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogsCur',
                aboutId: 'about',
                article: art,
                imageSrc: "../data/fs/" + art.imageSrc,
                id: req.params.id
            });
        }
    });
});

module.exports = router;