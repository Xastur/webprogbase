const express = require('express');
const users = require('../models/user');
const usersJSON = require('../data/users');
const router = express.Router();

router.get('/public/images/:name', (req, res) => {
    const path = "/public/images/" + req.params.name;
    res.sendFile('/home/dima/webprogbase/labs/lab4/' + path);
})

router.get('/users', function(req, res) {
    res.render('users', {title: 'Users',
                         homeId: 'home',
                         usersId: 'usersCur',
                         blogsId: 'blogs',
                         aboutId: 'about',
                         users: usersJSON});
});

router.get('/public/images/:name', (req, res) => {
    const path = "/public/images/" + req.params.name;
    res.sendFile('/home/dima/webprogbase/labs/lab4/' + path);
})

router.get('/users/:id', (req, res) => {
    users.getById(parseInt(req.params.id), function(user, err) {
        if (err)
        {
            console.log(err.toString());
            res.status(500).send(err.toString());
        }
        else 
        {
            res.render('user', {title: 'User' + req.params.id,
                                            homeId: 'home',
                                            usersId: 'usersCur',
                                            blogsId: 'blogs',
                                            aboutId: 'about',
                                            imageSrc: "../public/images/" + user.avaSrc,
                                            user: user});  
        }
    });
});

module.exports = router;