const express = require('express');
const users = require('../models/user');
const router = express.Router();

router.get('/users', function(req, res) {
    users.getAll(function(usrs, err) {
        res.end(JSON.stringify(usrs, null, 4));
    });
});

router.get('/users/:id', function(req, res) {
    users.getById(parseInt(req.params.id), function(usr, err) {
        if (err)
        {
            console.log(err.toString());
            res.status(404).send("Error 404");
        }
        else 
        {
            res.end(JSON.stringify(usr, null, 4));
        }
    });
});

module.exports = router;