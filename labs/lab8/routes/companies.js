let multer = require("multer");
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;
const Company = require('../models/company');
const User = require('../models/user');
var mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const cloudinary = require('cloudinary');
const dbconfig = require('../config/database');

cloudinary.config({
    cloud_name: dbconfig.cloud_name,
    api_key: dbconfig.api_key,
    api_secret: dbconfig.api_secret
});

var storage = multer.diskStorage({})
var upload = multer({ storage: storage })

router.get('/companies', isLoggedIn, function (req, res) {
    Company.getAll()
        .then(comps => {
            let userLogin;
            let admin = false;
            if (req.user) {
                userLogin = req.user.login;
                if (req.user.role === 1)
                    admin = true;
            }

            res.render('companies', {
                title: 'Companies',
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogs',
                companiesId: 'companiesCur',
                aboutId: 'about',
                companies: comps,
                user: req.user,
                userLogin: userLogin,
                admin: admin,
                pageId: req.user._id
            });

        })
        .catch(err => {
            console.log(err);
            res.redirect('/');
        })
});

router.get('/companies/new', isLoggedIn, (req, res) => {
    let userLogin;
    let admin = false;
    if (req.user) {
        userLogin = req.user.login;
        if (req.user.role === 1)
            admin = true;
    }

    res.render('newCompany', {
        title: "New Company",
        homeId: 'home',
        usersId: 'users',
        blogsId: 'blogs',
        companiesId: 'companiesCur',
        aboutId: 'about',
        user: req.user,
        userLogin: userLogin,
        admin: admin,
        pageId: req.user._id
    });
});

router.post('/companies/new', upload.single('imageSrc'), (req, res) => {
    const date = new Date();
    date.toISOString();
    const file = req.file;
    if (!file || (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') || req.body.companyName === '') {
        console.log("Wrong file");
        return res.redirect('/companies/new');
    }

    cloudinary.v2.uploader.upload(file.path, function (err, data) {
        if (err) {
            console.log(err);
            res.redirect('/');
        }

        let newCompany = new Company(
            new mongoose.Types.ObjectId(),
            req.body.companyName,
            req.user._id,
            [],
            req.body.aboutCompany,
            date,
            data.url
        );

        Company.insert(newCompany)
            .then(x => {
                return User.update(req.user._id, { $push: { 'companiesId': x._id } })
            })
            .then(x => res.redirect(newCompany._id))
            .catch(err => {
                console.log(err);
                res.redirect('/');
            })
    })
});

router.get('/companies/:id', isLoggedIn, (req, res) => {
    const companyId = req.params.id;
    Company.getCompany(companyId)
        .then(comp => {
            let userLogin;
            let admin = false;
            if (req.user) {
                userLogin = req.user.login;
                if (req.user.role === 1)
                    admin = true;
            }

            User.findById(comp.admins) 
                .then(creator => {
                    if (JSON.stringify(creator._id) === JSON.stringify(req.user._id))
                        admin = true;

                        res.render('company', {
                            title: comp.companyName,
                            homeId: 'home',
                            usersId: 'users',
                            blogsId: 'blogs',
                            companiesId: 'companiesCur',
                            aboutId: 'about',
                            company: comp,
                            id: ObjectId(companyId),
                            user: req.user,
                            userLogin: userLogin,
                            admin: admin,
                            pageId: req.user._id
                        });
                })
                .catch(err => res.redirect('/error/' + err))

                
        })
        .catch(err => {
            console.log(err);
            res.redirect('/');
        })
});

router.post('/companies/:id', (req, res) => {
    const companyId = req.params.id;
    Company.delete(companyId)
        .then(x => {
            console.log('Successfully deleted a company');
            res.redirect("/companies");
        })
        .catch(err => {
            console.log(err);
            res.redirect("/");
        })

});

router.get('/companies/:id/update', isLoggedIn, (req, res) => {
    const companyId = req.params.id;
    Company.getCompany(companyId)
        .then(comp => {
            let userLogin;
            let admin = false;
            if (req.user) {
                userLogin = req.user.login;
                if (req.user.role === 1)
                    admin = true;
            }

            res.render('updateCompany', {
                title: comp.companyName,
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogs',
                companiesId: 'companiesCur',
                aboutId: 'about',
                company: comp,
                user: req.user,
                userLogin: userLogin,
                admin: admin,
                pageId: req.user._id
            })
        })
        .catch(err => {
            console.log(err);
            res.redirect("/");
        })
})

router.post('/companies/:id/update', upload.single('imageSrc'), (req, res) => {
    const companyId = req.params.id;
    let updateInfo;
    const file = req.file;
    if (file) {
        cloudinary.v2.uploader.upload(file.path, function (err, data) {
            if (err) {
                console.log(err);
                res.redirect('/');
            }

            updateInfo = {
                imageSrc: data.url,
                companyName: req.body.companyName,
                aboutCompany: req.body.aboutCompany
            }

            Company.update(companyId, updateInfo)
                .then(x => {
                    console.log("Updated company");
                    res.redirect('/companies/' + companyId);
                })
                .catch(err => {
                    console.log("Error: " + err);
                    res.redirect('/');
                })
        })
    }
    else {
        updateInfo = {
            companyName: req.body.companyName,
            aboutCompany: req.body.aboutCompany
        }

        Company.update(companyId, updateInfo)
            .then(x => {
                console.log("Updated company");
                res.redirect('/companies/' + companyId);
            })
            .catch(err => {
                console.log("Error: " + err);
                res.redirect('/');
            })
    }
})

// router('/companies/:id/new', (req, res) => {

// })

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/auth/login');
}

module.exports = router;