const express = require('express');
const router = express.Router();

router.get('/', function (req, res) {
    let userLogin;
    let admin = false;
    let pageId;
    if (req.user) {
        userLogin = req.user.login;
        pageId = req.user._id;
        if (req.user.role === 1)
            admin = true;
    }

    res.render('index', {
        title: 'Home',
        homeId: 'homeCur',
        usersId: 'users',
        blogsId: 'blogs',
        companiesId: 'companies',
        aboutId: 'about',
        user: req.user,
        userLogin: userLogin,
        admin: admin, 
        pageId: pageId
    });
});

module.exports = router;