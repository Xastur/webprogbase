const express = require('express');
var mongoose = require('mongoose');
const router = express.Router();
const User = require('../models/user');
const Article = require('../models/article');
const Company = require('../models/company');
let multer = require("multer");
const cloudinary = require('cloudinary');
const dbconfig = require('../config/database');

const passport = require('../app');

var storage = multer.diskStorage({})
var upload = multer({ storage: storage })

cloudinary.config({
    cloud_name: dbconfig.cloud_name,
    api_key: dbconfig.api_key,
    api_secret: dbconfig.api_secret
});

router.get('/me', passport.authenticate('basic', { session: false }), function (req, res) {
    res.status(200).json(req.user);
})

router.get('/', function (req, res) {
    res.status(200).json("Empty json");
})

router.get('/users', passport.authenticate('basic', { session: false }), function (req, res) {
    if (req.user.role !== 1)
        return res.status(403).send("Forbidden");

    const currPage = parseInt(req.query.page);
    const paginationAmount = 1;

    User.getAllPaginated((currPage - 1) * paginationAmount, paginationAmount)
        .then(users => {
            res.status(200).json(users);
        })
        .catch(err => {
            res.status(400).send(err);
        })
})

router.get('/users/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const userId = req.params.id;

    User.findById(userId)
        .then(user => res.status(200).json(user))
        .catch(err => res.status(500).send(err))
})

router.post('/users', function (req, res) {

    User.findOne({ 'login': req.body.login })
        .then(user => {
            if (user && !user.isDisabled) res.send("Email already taken")
            else if (req.body.password !== req.body.password2) res.send("Passwords not match")
            else if (req.body.password.length < 5) res.send('Password length < 5')
            else if (req.body.login.length < 4) res.send('Login length < 4')
            else if (req.body.fullname.length < 2) res.send('Fullname length < 2')
            else {
                let newUser = new User();

                newUser._id = new mongoose.Types.ObjectId();
                newUser.articlesId = [];
                newUser.companiesId = [];
                newUser.login = req.body.login;
                newUser.password = newUser.generateHash(req.body.password);
                newUser.fullname = req.body.fullname;
                newUser.biography = req.body.biography;
                newUser.role = 0;
                newUser.registeredAt = new Date().toISOString();
                newUser.avaUrl = "https://res.cloudinary.com/hhiefmflq/image/upload/v1572597465/sample.jpg";
                newUser.isDisabled = false;

                return User.insert(newUser);
            }
        })
        .then(x => res.status(200).send(x))
        .catch(err => res.status(400).send(err))
})

router.put('/users/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const userId = req.params.id;

    if (userId != req.user._id && req.user.role != 1)
        return res.status(403).send("Forbidden");

    User.findById(userId)
        .then(user => {
            let login = user.login;
            let articlesId = user.articlesId;
            let companiesId = user.companiesId;
            let fullname = user.fullname;
            let biography = user.biography;
            let role = user.role;
            let isDisabled = user.isDisabled;

            if (req.body.login) login = req.body.login;
            if (req.body.articlesId) articlesId = req.body.articlesId;
            if (req.body.companiesId) companiesId = req.body.companiesId;
            if (req.body.fullname) fullname = req.body.fullname;
            if (req.body.biography) biography = req.body.biography;
            if (req.body.role) role = req.body.role;
            if (req.body.isDisabled) isDisabled = req.body.isDisabled;

            return User.update(userId, {
                'login': login,
                'articlesId': articlesId,
                'companiesId': companiesId,
                'fullname': fullname,
                'biography': biography,
                'role': role,
                'isDisabled': isDisabled
            })
        })
        .then(x => res.status(200).json(x))
        .catch(err => res.status(400).send(err))
})

router.delete('/users/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const userId = req.params.id;

    if (userId != req.user._id && req.user.role != 1)
        return res.status(403).send("Forbidden");

    User.getById(userId)
        .then(user => {
            let p1 = Article.deleteMany(user.articlesId);
            let p2 = User.update(userId, { 'isDisabled': true });
            return Promise.all([p1, p2]);
        })
        .then(x => res.status(200).send(x))
        .catch(err => res.status(400).send(err))
})

router.get('/blogs', passport.authenticate('basic', { session: false }), function (req, res) {
    const currPage = parseInt(req.query.page);
    const paginationAmount = 1;

    Article.getAllPaginated((currPage - 1) * paginationAmount, paginationAmount)
        .then(blogs => res.status(200).json(blogs))
        .catch(err => res.status(400).send(err))
})

router.get('/blogs/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const blogId = req.params.id;

    Article.getArticle(blogId)
        .then(blog => res.status(200).json(blog))
        .catch(err => res.status(204).send(err))
})

router.post('/blogs', passport.authenticate('basic', { session: false }), function (req, res) {
    const date = new Date();

    let newBlog = new Article();
    newBlog._id = new mongoose.Types.ObjectId();
    newBlog.artTitle = req.body.artTitle;
    newBlog.userId = req.user._id;
    newBlog.companyId = req.body.companyId;
    newBlog.aboutMyCar = req.body.aboutMyCar;
    newBlog.passportData = req.body.passportData;
    newBlog.likesAmount = 0;
    newBlog.viewsAmount = 0;
    newBlog.data = date;
    newBlog.imageSrc = "https://res.cloudinary.com/hhiefmflq/image/upload/v1572597465/sample.jpg";

    if (newBlog.aboutMyCar == undefined)
        newBlog.aboutMyCar = '';
    if (newBlog.passportData == undefined)
        newBlog.passportData = '';

    Article.insert(newBlog)
        .then(x => {
            return User.update(req.user._id, { $push: { 'articlesId': x._id } })
        })
        .then(x => res.status(201).json(newBlog))
        .catch(err => res.status(400).send(err))
})

router.put('/blogs/:id', upload.single('ImageSrc'), passport.authenticate('basic', { session: false }), function (req, res) {
    const blogId = req.params.id;

    if (req.user.articlesId.length == 0)
        return res.status(403).send("Forbidden");

    for (let i = 0; i < req.user.articlesId.length; i++) {
        if (req.user.articlesId[i] == blogId)
            break;

        if (i == req.user.articlesId.length - 1)
            return res.status(403).send("Forbidden");
    }
    if (req.body.ArtTitle === '')
        return res.status(400).send("Not enough info");

    let file;
    if (req.file) {
        file = req.file
        if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg')
            return res.status(400).send("Wrong file");
        cloudinary.v2.uploader.upload(file.path, function (err, data) {
            Article.getArticle(blogId)
                .then(article => {
                    let artTitle = article.artTitle;
                    let aboutMyCar = article.aboutMyCar;
                    let passportData = article.passportData;
                    let likesAmount = article.likesAmount;
                    let viewsAmount = article.viewsAmount;

                    if (req.body.artTitle) artTitle = req.body.artTitle;
                    if (req.body.aboutMyCar) aboutMyCar = req.body.aboutMyCar;
                    if (req.body.passportData) passportData = req.body.passportData;
                    if (req.body.likesAmount) likesAmount = req.body.likesAmount;
                    if (req.body.viewsAmount) viewsAmount = req.body.viewsAmount;

                    Article.update(blogId, {
                        'artTitle': artTitle,
                        'aboutMyCar': aboutMyCar,
                        'passportData': passportData,
                        'likesAmount': likesAmount,
                        'viewsAmount': viewsAmount,
                        'imageSrc': data.url
                    })
                        .then(x => res.status(200).json(x))
                        .catch(err => res.status(400).send(err))
                })
        })
    }
    else {
        Article.getArticle(blogId)
            .then(article => {
                let artTitle = article.artTitle;
                let aboutMyCar = article.aboutMyCar;
                let passportData = article.passportData;
                let likesAmount = article.likesAmount;
                let viewsAmount = article.viewsAmount;

                if (req.body.artTitle) artTitle = req.body.artTitle;
                if (req.body.aboutMyCar) aboutMyCar = req.body.aboutMyCar;
                if (req.body.passportData) passportData = req.body.passportData;
                if (req.body.likesAmount) likesAmount = req.body.likesAmount;
                if (req.body.viewsAmount) viewsAmount = req.body.viewsAmount;

                Article.update(blogId, {
                    'artTitle': artTitle,
                    'aboutMyCar': aboutMyCar,
                    'passportData': passportData,
                    'likesAmount': likesAmount,
                    'viewsAmount': viewsAmount,
                })
                    .then(x => res.status(200).json(x))
                    .catch(err => res.status(400).send(err))
            })
    }

})

router.delete('/blogs/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const blogId = req.params.id;

    for (let i = 0; i < req.user.articlesId.length; i++) {
        if (req.user.articlesId[i] == blogId)
            break;

        if (i == req.user.articlesId.length - 1)
            return res.status(403).send("Forbidden");
    }

    Article.delete(blogId)
        .then(x => {
            return User.update(req.user._id, { $pull: { 'articlesId': x._id } })
        })
        .then(x => res.status(200).json("Successfully deleted an article"))
        .catch(err => res.status(400).send(err))
})

router.get('/companies', passport.authenticate('basic', { session: false }), function (req, res) {
    const currPage = parseInt(req.query.page);
    const paginationAmount = 1;

    Company.getAllPaginated((currPage - 1) * paginationAmount, paginationAmount)
        .then(comps => res.status(200).json(comps))
        .catch(err => res.status(400).send(err))
})

router.get('/companies/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const companyId = req.params.id;

    Company.getCompany(companyId)
        .then(company => res.status(200).json(company))
        .catch(err => res.status(204).send(err))
})

router.post('/companies', passport.authenticate('basic', { session: false }), function (req, res) {
    const date = new Date().toISOString();

    let newCompany = new Company();
    newCompany._id = new mongoose.Types.ObjectId();
    newCompany.companyName = req.body.companyName;
    newCompany.admins = req.user._id;
    newCompany.articlesId = req.body.companyId;
    newCompany.aboutCompany = req.body.aboutCompany;
    newCompany.date = date;
    newCompany.imageSrc = "https://res.cloudinary.com/hhiefmflq/image/upload/v1572597465/sample.jpg";

    Company.insert(newCompany)
        .then(x => {
            return User.update(req.user._id, { $push: { 'companiesId': x._id } })
        })
        .then(x => res.status(201).json(newCompany))
        .catch(err => res.status(400).send(err))
})

router.put('/companies/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const companyId = req.params.id;

    Company.getCompany(companyId)
        .then(company => {
            let aboutCompany = company.aboutCompany;
            let companyName = company.companyName;
            let admins = company.admins;
            let articlesId = company.articlesId;
            let imageSrc = company.imageSrc;

            if (req.body.aboutCompany) aboutCompany = req.body.aboutCompany;
            if (req.body.companyName) companyName = req.body.companyName;
            if (req.body.admins) admins.push(req.body.admins);
            if (req.body.articlesId) articlesId.push(req.body.articlesId);
            if (req.body.imageSrc) imageSrc = req.body.imageSrc;

            return Company.update(companyId, {
                'aboutCompany': aboutCompany,
                'companyName': companyName,
                'admins': admins,
                'articlesId': articlesId,
                'imageSrc': imageSrc,
            })
        })
        .then(x => res.status(200).json(x))
        .catch(err => res.status(400).send(err))
})

router.delete('/companies/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    const companyId = req.params.id;

    for (let i = 0; i < req.user.companiesId.length; i++) {
        if (req.user.companiesId[i] == companyId)
            break;
        if (i == req.user.companiesId.length - 1)
            return res.status(403).send("Forbidden");
    }

    Company.getCompany(companyId)
        .then(company => {
            let p1 = User.updateMany({ 'companiesId': company._id }, { $pull: { 'companiesId': company._id } })
            let p2 = Article.deleteMany(company.articlesId);
            let p3 = Company.delete(company._id);
            return Promise.all([p1, p2, p3]);
        })
        .then(x => {
            res.status(200).json(x);
        })
        .catch(err => {
            res.status(400).send(err);
        })
})

module.exports = router;