var mongoose = require('mongoose');

var ArticleSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    artTitle: mongoose.Schema.Types.String,
    userId: mongoose.Schema.Types.ObjectId,
    companyId: mongoose.Schema.Types.ObjectId,
    aboutMyCar: mongoose.Schema.Types.String,
    passportData: mongoose.Schema.Types.String,
    likesAmount: Number,
    viewsAmount: Number,
    date: Date,
    imageSrc: mongoose.Schema.Types.String
});
 
let ArticleModel = mongoose.model('article', ArticleSchema);

class Article {
    constructor (_id, artTitle, companyId, userId, aboutMyCar, passportData, likesAmount, viewsAmount, date, imageSrc) {
        this._id = _id;
        this.artTitle = artTitle;
        this.userId = userId,
        this.companyId = companyId;
        this.aboutMyCar = aboutMyCar;
        this.passportData = passportData;
        this.likesAmount = likesAmount;
        this.viewsAmount = viewsAmount;
        this.date = date;
        this.imageSrc = imageSrc;
    }

    static getAll() {
        return ArticleModel.find();
    }

    static getAllPaginated(skip, limit) {
        return ArticleModel.find(null, null, { skip, limit });
    }

    static getArticle(id) {
        return ArticleModel.findById(id);
    }

    static insert(article) {
        return new ArticleModel(article).save();
    }

    static getAmount() {
        return ArticleModel.estimatedDocumentCount();
    }

    static delete(id) {
        return ArticleModel.findByIdAndDelete(id);
    }

    static update(id, updateInfo) {
        return ArticleModel.findOneAndUpdate({_id: id}, updateInfo);
    }
}

module.exports = Article;