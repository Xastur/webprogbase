var mongoose = require('mongoose');

var UserShema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    login: String,
    fullname: String,
    role: Number,
    registeredAt: Date,
    avaUrl: String,
    isDisabled: Boolean,
    companiesId: Array
});

const UserModel = mongoose.model('User', UserShema);

class User 
{
    constructor(login, role, fullname, avaUrl, registeredAt, companiesId) {
        this.login = login; // string
        this.fullname = fullname; // string
        this.role = role; // number
        this.registeredAt = registeredAt; // ISO 8601
        this.avaUrl = avaUrl; // str
        this.isDisabled = false; // bool
        this.companiesId = companiesId;
    }

    static getAll() {
        return UserModel.find();
    }

    static getUser(id) {
        return UserModel.findById(id);
    }

    static insert(user) {
        return new UserModel(user).save();
    }
};

module.exports = User;