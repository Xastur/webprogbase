let multer = require("multer");
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;
const Company = require('../models/company');
var mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const cloudinary = require('cloudinary');
const config = require('../config');

cloudinary.config({
    cloud_name: config.cloud_name,
    api_key: config.api_key,
    api_secret: config.api_secret
});

var storage = multer.diskStorage({})
var upload = multer({ storage: storage })

router.get('/companies', function (req, res) {
    Company.getAll()
        .then(comps => {
            res.render('companies', {
                title: 'Companies',
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogs',
                companiesId: 'companiesCur',
                aboutId: 'about',
                companies: comps,
            });

        })
        .catch(err => {
            console.log(err);
            res.redirect('/');
        })
});

router.get('/companies/new', (req, res) => {
    res.render('newCompany', {
        title: "New Company",
        homeId: 'home',
        usersId: 'users',
        blogsId: 'blogs',
        companiesId: 'companiesCur',
        aboutId: 'about',
    });
});

router.post('/companies/new', upload.single('imageSrc'), (req, res) => {
    const date = new Date();
    date.toISOString();
    const file = req.file;
    if (!file || (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') || req.body.companyName === '') {
        console.log("Wrong file");
        return res.redirect('/companies/new');
    }

    cloudinary.v2.uploader.upload(file.path, function (err, data) {
        if (err) {
            console.log(err);
            res.redirect('/');
        }

        let newCompany = new Company(
            new mongoose.Types.ObjectId(),
            req.body.companyName,
            null,
            null,
            req.body.aboutCompany,
            date,
            data.url
        );

        Company.insert(newCompany)
            .then(x => {
                console.log("Successfully inserted a company");
                res.redirect(newCompany._id);
            })
            .catch(err => {
                console.log(err);
                res.redirect('/');
            })
    })
});

router.get('/companies/:id', (req, res) => {
    const companyId = req.params.id;
    Company.getCompany(companyId)
        .then(comp => {
            res.render('company', {
                title: comp.companyName,
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogs',
                companiesId: 'companiesCur',
                aboutId: 'about',
                company: comp,
                id: ObjectId(companyId)
            });
        })
        .catch(err => {
            console.log(err);
            res.redirect('/');
        })
});

router.post('/companies/:id', (req, res) => {
    const companyId = req.params.id;
    Company.delete(companyId)
        .then(x => {
            console.log('Successfully deleted a company');
        })
        .catch(err => {
            console.log(err);
        })

    res.redirect("/companies");
});

router.get('/companies/:id/update', (req, res) => {
    const companyId = req.params.id;
    Company.getCompany(companyId)
        .then(comp => {
            res.render('updateCompany', {
                title: comp.companyName,
                homeId: 'home',
                usersId: 'users',
                blogsId: 'blogs',
                companiesId: 'companiesCur',
                aboutId: 'about',
                company: comp,
            })
        })
})

router.post('/companies/:id/update', upload.single('imageSrc'), (req, res) => {
    const companyId = req.params.id;
    let updateInfo;
    const file = req.file;
    if (file) {
        cloudinary.v2.uploader.upload(file.path, function (err, data) {
            if (err) {
                console.log(err);
                res.redirect('/');
            }

            updateInfo = {
                imageSrc: data.url,
                companyName: req.body.companyName,
                aboutCompany: req.body.aboutCompany
            }

            Company.update(companyId, updateInfo)
            .then(x => {
                console.log("Updated company");
                res.redirect('/companies/' + companyId);
            })
            .catch(err => {
                console.log("Error: " + err);
                res.redirect('/');
            })
        })
    }
    else {
        updateInfo = {
            companyName: req.body.companyName,
            aboutCompany: req.body.aboutCompany
        }

        Company.update(companyId, updateInfo)
        .then(x => {
            console.log("Updated company");
            res.redirect('/companies/' + companyId);
        })
        .catch(err => {
            console.log("Error: " + err);
            res.redirect('/');
        })
    }
})

module.exports = router;