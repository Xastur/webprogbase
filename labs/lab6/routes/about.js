const express = require('express');
const router = express.Router();

router.get('/about', function(req, res, next) {
    res.render('about', {title: 'About',
                         homeId: 'home',
                         usersId: 'users',
                         blogsId: 'blogs',
                         companiesId: 'companies',
                         aboutId: 'aboutCur'});
});

module.exports = router;